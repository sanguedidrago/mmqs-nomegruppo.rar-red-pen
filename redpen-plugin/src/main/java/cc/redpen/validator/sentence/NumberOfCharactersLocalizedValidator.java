package cc.redpen.validator.sentence;

import cc.redpen.model.Sentence;
import cc.redpen.validator.Validator;

public class NumberOfCharactersLocalizedValidator extends Validator {

    @Override
    public void validate(Sentence sentence) {
        int minLenght = 100;
        if (sentence.getContent().length() < minLenght) {
            // actual error message is in NumberOfCharactersLocalizedValidator.properties
            addLocalizedError(sentence, minLenght);
        }
        int maxLenght = 1000;
        if (sentence.getContent().length() > maxLenght) {
            // You can specify a message key when you have multiple error message variations
            addLocalizedError("toolong", sentence, maxLenght);
        }
    }
}
