package cc.redpen.validator.sentence;

import cc.redpen.model.Sentence;
import cc.redpen.validator.Validator;

import java.util.Objects;

public class NumberOfCharactersValidator extends Validator {
    private static final int MIN_LENGTH = 100;
    private static final int MAX_LENGTH = 1000;

    @Override
    public void validate(Sentence sentence) {
        if (sentence.getContent().length() < MIN_LENGTH) {
            addError("Sentence is shorter than " + MIN_LENGTH + " characters long.", sentence);
        }
        if (sentence.getContent().length() > MAX_LENGTH) {
            addError("Sentence is longer than " + MAX_LENGTH + " characters long.", sentence);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NumberOfCharactersValidator)) return false;
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), MIN_LENGTH, MAX_LENGTH);
    }
}
