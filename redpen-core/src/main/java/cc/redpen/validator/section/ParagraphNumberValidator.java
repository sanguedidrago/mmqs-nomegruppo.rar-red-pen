/**
 * redpen: a text inspection tool
 * Copyright (c) 2014-2015 Recruit Technologies Co., Ltd. and contributors
 * (see CONTRIBUTORS.md)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.redpen.validator.section;

import cc.redpen.model.Section;
import cc.redpen.validator.Validator;

import java.util.Objects;

/**
 * Validate paragraph number. If a section has paragraphs more than specified,
 * This validator reports it.
 */
public final class ParagraphNumberValidator extends Validator {
    String max = "max_num";
    public ParagraphNumberValidator() {
        super("max_num", 5); // Default maximum number of paragraphs in a section.
    }

    @Override
    public void validate(Section section) {
        int paragraphNumber = section.getNumberOfParagraphs();
        if (getInt(max) < paragraphNumber) {
            addLocalizedError(section.getJoinedHeaderContents(), getInt(max));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParagraphNumberValidator)) return false;
        if (!super.equals(o)) return false;
        ParagraphNumberValidator that = (ParagraphNumberValidator) o;
        return Objects.equals(max, that.max);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), max);
    }
}
