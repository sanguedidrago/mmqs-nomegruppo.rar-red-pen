/**
 * redpen: a text inspection tool
 * Copyright (c) 2014-2015 Recruit Technologies Co., Ltd. and contributors
 * (see CONTRIBUTORS.md)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.redpen.validator.sentence;


import cc.redpen.model.Sentence;
import cc.redpen.tokenizer.TokenElement;

import java.util.Arrays;

/**
 * Ensure groups of words that are hyphenated in the dictionary are hyphenated in the sentence
 */
public class HyphenationValidator extends SpellingDictionaryValidator {

    TokenElement[] tokens = new TokenElement[]{null, null, null, null};

    @Override
    public void validate(Sentence sentence) {
        // consider hyphenated words of this array's length


        // combine sequences of tokens together with hyphens and test
        // for their presence in the dictionary
        for (int i = 0; i < sentence.getTokens().size(); i++) {
            for (int j = 0; j < tokens.length - 1; j++) {
                tokens[j] = i + j < sentence.getTokens().size() ? sentence.getTokens().get(i + j) : null;
            }

            voidTokens(sentence);
        }
    }

    private void voidTokens(Sentence sentence) {
        if (tokens[0] != null) {
            // check all groups to see if they are tokenized in the dictionary
            StringBuilder hyphenatedForm = new StringBuilder(tokens[0].getSurface());
            for (int j = 1; (j < tokens.length) && (tokens[j] != null); j++) {
                hyphenatedForm.append("-").append(tokens[j].getSurface());
                if (inDictionary(hyphenatedForm.toString().toLowerCase())) {
                    addLocalizedErrorWithPosition(
                            "HyphenatedInDictionary",
                            sentence,
                            tokens[0].getOffset(),
                            tokens[j].getOffset() + tokens[j].getSurface().length(),
                            hyphenatedForm.toString());
                    break;
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        HyphenationValidator that = (HyphenationValidator) o;
        return Arrays.equals(tokens, that.tokens);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(tokens);
        return result;
    }
}
