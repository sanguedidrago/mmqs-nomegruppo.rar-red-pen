package cc.redpen.parser;

import java.util.Map;

public class Mapp
{
    Map<String, DocumentParser> parserMap;

    public Map<String, DocumentParser> getParserMap() {
        return parserMap;
    }

    public void setParserMap(Map<String, DocumentParser> parserMap) {
        this.parserMap = parserMap;
    }

}
