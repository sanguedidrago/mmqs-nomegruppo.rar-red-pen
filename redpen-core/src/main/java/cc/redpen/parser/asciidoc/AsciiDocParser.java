/**
 * redpen: a text inspection tool
 * Copyright (c) 2014-2015 Recruit Technologies Co., Ltd. and contributors
 * (see CONTRIBUTORS.md)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.redpen.parser.asciidoc;

import cc.redpen.parser.PreprocessingReader;
import cc.redpen.parser.common.Line;
import cc.redpen.parser.common.LineParser;
import cc.redpen.parser.common.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;


/**
 * Erasing parser for the AsciiDoc format<br>
 * <p>
 * One of the requirements for RedPen is that the line &amp; column position (ie: offset) for
 * each character in the parsed text be preserved throughout parsing and validation.
 * <p>
 * This AsciiDoc parser attempts to solve this requirement by maintaining a model of the source
 * document's characters and their original position, and then logically 'erasing' the parts of
 * that model - usually the markup - that should not be presented to RedPen for validation.
 * <p>
 * The remaining "un-erased" text is transformed into RedPen's document model.
 * <p>
 * AsciiDoc's syntax and grammar is documented at @see http://asciidoc.org/
 */
public class AsciiDocParser extends LineParser {

    private static final Logger LOG = LoggerFactory.getLogger(AsciiDocParser.class);

    /**
     * An array of AsciiDoctor macros to erase
     */
    private static final String[] MACROS = {
            "ifdef::",
            "ifndef::",
            "ifeval::",
            "endif::",
    };

    /**
     * An array of AsciiDoc admonitions to erase
     */
    private static final String[] ADMONITIONS = {
            "NOTE: ",
            "TIP: ",
            "IMPORTANT: ",
            "CAUTION: ",
            "WARNING: "
    };

    /**
     * An array of AsciiDoc external link prefixes
     */
    private static final String[] EXTERNAL_LINK_PREFIXES = {
            "link:",
            "http://",
            "https://",
            "include:"
    };

    /**
     * current parser state
     */
    private class State {
        // are we in a block
        private boolean inBlock = false;
        // are we in a list?
        private boolean inList = false;
        // should we erase lines within the current block?
        private boolean eraseBlock = true;
        // the sort of block we are in
        private char blockMarker = 0;
        // length of the lead block marker
        private int blockMarkerLength = 0;
    }


    /**
     * populate the erasable model with the text from the inputstream
     * @param model model to populate
     * @param io stream to read
     */
    protected void populateModel(Model model, InputStream io) {

        try (PreprocessingReader reader = createReader(io)) {
            int lineno = 0;
            // add the lines to the model
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                lineno++;
                model.add(new AsciiDocLine(line, lineno));
            }

            model.setPreprocessorRules(reader.getPreprocessorRules());

            // process each line of the model
            State state = new State();
            for (model.rewind(); model.isMore(); model.getNextLine()) {
                processLine(model.getCurrentLine(), model, state);
            }

            processHeader(model);

        } catch (Exception e) {
            LOG.error("Exception when parsing AsciiDoc file", e);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("AsciiDoc parser model (X=erased line,[=block,section-listlevel-lineno,*=list item):\n {}", model);
        }
    }

    /**
     * Does the give line start a list?
     *
     * @param line
     * @param nextLine the subsequent line
     * @return
     */
    private boolean isListElement(Line line, Line nextLine) {

        int pos = 0;
        while (Character.isWhitespace(line.charAt(pos))) {
            pos++;
        }

        // test for labelled lists
        if ((line.charAt(line.length() - 1) == ':') &&
                (line.charAt(line.length() - 2) == ':')) {
            int level = 1;
            pos = line.length() - 3;
            while ((pos > 0) && line.charAt(pos) == ':') {
                pos--;
                level++;
            }
            nextLine.setListLevel(level);
            nextLine.setListStart(true);
            line.erase();
            return true;
        }

        // test for single line labeled list
        // NOTE: single line labeled list must be level 1
        int position = 0;
        if ((position = line.getText().indexOf(":: ")) != -1) {
            line.setListLevel(1);
            line.setListStart(true);
            line.erase(0, position+3);
            return true;
        }
        return false;
    }

    /**
     * Process the header, erasing the annotations that can follow it
     *
     * @param model
     */
    private void processHeader(Model model) {
        // look for "= Text of header" or "Text of header\n==========="
        if (model.lineCount() > 1) {
            boolean haveHeader = false;
            if ((model.getLine(1).charAt(0, true) == '=')
                    && (model.getLine(1).charAt(1, true) == ' ')
                    && (model.getLine(1).length() == model.getLine(2).length())
                    && model.getLine(2).isAllSameCharacter()
                    && (model.getLine(2).charAt(0, true) == '=')) {
                haveHeader = true;
            }

            if (haveHeader) {
                model.getLine(1).setSectionLevel(1);
                // erase lines up until an empty line
                for (int i = 2; i <= model.lineCount(); i++) {
                    if (!model.getLine(i).isErased() && model.getLine(i).isEmpty()) {
                        break;
                    }
                    model.getLine(i).erase();
                }
            }
        }
    }

    /**
     * Process the current line, removing asciidoc tags and markup and setting the current state
     *
     * @param line
     * @param state the current state
     */
    private void processLine(Line line, Model model, State state) {
        if (line.isErased()) { return; }

        TargetLine target = new TargetLine(line,
                model.getLine(line.getLineNo() - 1),
                model.getLine(line.getLineNo() + 1));

        if (state.inList && (line.getListLevel() == 0)) {
            line.setListLevel(target.getPreviousLine().getListLevel());
        }

       endBlock(state, line, target);

        // check for old style heading (line followed by single-char-line of same length)
        if (line.isAllSameCharacter() &&
                (line.length() == target.getPreviousLine().length()) &&
                ("=-~^+".indexOf(target.getFirstChar()) != -1) &&
                (". [".indexOf(target.getPreviousLine().charAt(0, true)) == -1)) {
            target.getPreviousLine().setSectionLevel(1);
            line.erase();
            return;
        }

        // horizontal rule
       horizontalRule(line);

        // block markers in which we process the internal text
        if (line.isAllSameCharacter() && (line.length() >= 4) &&
                ("_*".indexOf(target.getFirstChar()) != -1)) {
            line.erase();
            return;
        }

        // test for various block starts
       testBlockStarts(state, line, target);

            // see if we are starting other types of blocks
          checkStarts(target, line, state);

            // see if this is a table marker
           checkInTableMarket(line, target, state);

       eraseCondition(state, target, line);

        // erase urls and links
        for (String prefix : EXTERNAL_LINK_PREFIXES) {
            line.eraseEnclosure(prefix, " ,[", AsciiDocLine.EraseStyle.CLOSEMARKERSCONTAINSDELIMITERS);
        }

        // handle images
       handleImages(line);

        int headerIndent = 0;
        while (line.charAt(headerIndent) == '=') {
            headerIndent++;
        }
        if ((headerIndent > 0) && (line.charAt(headerIndent) == ' ')) {
            line.erase(0, headerIndent + 1);
            line.setSectionLevel(headerIndent);
        }

        // lists!
        if (!state.inBlock && isListElement(line, target.getNextLine())) {
            state.inList = true;
        }

        // continuation markers
        continueMarkers(line);

        // a blank line will cancel any list element we are in
        if (state.inList && (line.length() == 0)) {
            state.inList = false;
            line.setListLevel(0);
        }

        // macros
        checkMacros(line);

        // admonitions
       checkAdmonitions(line);

        eraseInlineMarkup(line);
    }

    public void testBlockStarts(State state, Line line, TargetLine target) {
        if (!state.inBlock && line.isAllSameCharacter() && (line.length() == 3) && (line.charAt(0) == '`')) {
            // fenced block
                state.inBlock = true;
                state.eraseBlock = true;
                state.blockMarker = target.getFirstChar();
                state.blockMarkerLength = line.length();
                line.setInBlock(true);
                line.erase();
            }
    }

    public void checkMacros(Line line){
        for (String macro : MACROS) {
            if (line.startsWith(macro)) {
                line.erase();
                break;
            }
        }
    }

    public void checkAdmonitions(Line line){
        for (String admonition : ADMONITIONS) {
            if (line.startsWith(admonition)) {
                line.erase(0, admonition.length());
                break;
            }
        }
    }

    public void horizontalRule(Line line){
        if (line.isAllSameCharacter() && (line.length() == 3) && (line.charAt(0) == '\'')) {
            line.erase();
        }
    }

    public void checkInTableMarket(Line line, TargetLine target, State state){
        if ((line.length() >= 4) && (target.getFirstChar() == '|') && (target.getSecondChar() == '=')) {
            line.erase();
            state.inBlock = true;
            state.eraseBlock = true;
            state.blockMarker = '|';
            state.blockMarkerLength = 1;
            line.setInBlock(true);
        }
    }

    public void endBlock(State state, Line line, TargetLine target){
        // check for block end
        if (state.inBlock) {
            if (line.isAllSameCharacter() &&
                    (target.getFirstChar() == state.blockMarker) &&
                    (line.length() == state.blockMarkerLength) &&(line.length() >= 4) &&
                    (target.getFirstChar() == '|') &&
                    (target.getSecondChar() == '=')) {
                // end a regular block
                line.erase();
                line.setInBlock(true);
                state.inBlock = false;
                return;
            }
            // erase the block content
            line.setInBlock(true);
            if (state.eraseBlock) {
                line.erase();
            }
        }
    }

    public void continueMarkers(Line line){
        if (line.charAt(line.length() - 1) == '+') {
            if (line.length() == 0) {
                line.erase();
            } else {
                line.erase(line.length() - 1, 1);
            }
        }
    }

    public void checkStarts(TargetLine target, Line line, State state){
        if (line.isAllSameCharacter() && (line.length() >= 4)) {
            switch (target.getFirstChar()) {
                case '-':
                case '=':
                case '&':
                case '/':
                case '+':
                case '.':
                    // blocks that have their innards erased
                    state.inBlock = true;
                    state.eraseBlock = true;
                    state.blockMarker = target.getFirstChar();
                    state.blockMarkerLength = line.length();
                    line.setInBlock(true);
                    line.erase();
                    return;
                default:
                    break;
            }
        }
    }

    public void handleImages(Line line){
        int position = line.eraseEnclosure("image:", " ,[", AsciiDocLine.EraseStyle.CLOSEMARKERSCONTAINSDELIMITERS);
        if (position != -1) {
            line.eraseEnclosure("[", "]", AsciiDocLine.EraseStyle.ALL); // TODO: extract "caption" and "title" as a sentence
        } else {
            // enclosed directives
            line.eraseEnclosure("+++", "+++", AsciiDocLine.EraseStyle.ALL);
            line.eraseEnclosure("[[", "]]", AsciiDocLine.EraseStyle.ALL);
            line.eraseEnclosure("<<", ">>", AsciiDocLine.EraseStyle.PRESERVABLE);
            line.eraseEnclosure("{", "}", AsciiDocLine.EraseStyle.MARKERS); // NOTE: should we make substitutions?
            line.eraseEnclosure("[", "]", AsciiDocLine.EraseStyle.MARKERS);
        }
    }

    public void eraseCondition(State state, TargetLine target, Line line){
        if (!state.inList && (target.getFirstChar() == ' ')) {
            line.erase();
            return;
        }

        // maybe we have a comment?
        if ((target.getFirstChar() == '/') && (target.getSecondChar() == '/')) {
            line.erase();
            return;
        }

        // 'open' block marker
        if ((target.getFirstChar() == '-') && (target.getSecondChar() == '-')) {
            line.erase();
            return;
        }

        // attributes (at position == 0)
        if (line.eraseEnclosure(":", ":", AsciiDocLine.EraseStyle.NONE) == 0) {
            line.erase();
            return;
        }
        if (line.eraseEnclosure("[", "]", AsciiDocLine.EraseStyle.NONE) == 0) {
            line.erase();
            return;
        }

        // check for a title
        if ((target.getFirstChar() == '.') && (" .".indexOf(target.getSecondChar()) == -1)) {
            line.erase(0, 1);
        }

    }


    /**
     * Erase all inline markup (bold, italics, special tokens etc)
     *
     * @param line
     */
    private void eraseInlineMarkup(Line line) {
        // inline markup (bold, italics etc)
        line.eraseEnclosure("__", "__", AsciiDocLine.EraseStyle.MARKERS);
        line.eraseEnclosure("**", "**", AsciiDocLine.EraseStyle.MARKERS);
        line.eraseEnclosure("``", "``", AsciiDocLine.EraseStyle.MARKERS);
        line.eraseEnclosure("##", "##", AsciiDocLine.EraseStyle.MARKERS);
        line.eraseEnclosure("^", "^", AsciiDocLine.EraseStyle.MARKERS);
        line.eraseEnclosure("~", "~", AsciiDocLine.EraseStyle.MARKERS);

        line.eraseEnclosure("_", "_", AsciiDocLine.EraseStyle.INLINEMARKUP);
        line.eraseEnclosure("*", "*", AsciiDocLine.EraseStyle.INLINEMARKUP);
        line.eraseEnclosure("`", "`", AsciiDocLine.EraseStyle.INLINEMARKUP);
        line.eraseEnclosure("#", "#", AsciiDocLine.EraseStyle.INLINEMARKUP);

        line.erase("'`");
        line.erase("`'");
        line.erase("\"`");
        line.erase("`\"");
        line.erase("(C)");
        line.erase("(R)");
        line.erase("(TM)");
    }
}
