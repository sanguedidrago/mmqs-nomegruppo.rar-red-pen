package cc.redpen.parser.rest;

import cc.redpen.parser.common.Line;

public class ReSTLine extends Line {
    /**
     * Construct a line using the supplied string
     *
     */
    public ReSTLine() {
        super();


        // trim the end
        while (!characters.isEmpty() &&
                Character.isWhitespace(characters.get(characters.size() - 1))) {
            characters.remove(characters.size() - 1);
        }
    }
}
