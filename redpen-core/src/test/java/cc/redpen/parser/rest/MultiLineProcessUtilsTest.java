package cc.redpen.parser.rest;

import cc.redpen.parser.common.LineParser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MultiLineProcessUtilsTest {
    @Test
    void testDetectThreeLineHeader() {
        LineParser.TargetLine target = new LineParser.TargetLine(
                new ReSTLine(),
                new ReSTLine(),
                new ReSTLine());
        assertEquals(true, MultiLineProcessUtils.processMultiLineMatch('#', '#', target));
    }

    @Test
    void testDetectTwoLineHeader() {
        LineParser.TargetLine target = new LineParser.TargetLine(
                new ReSTLine(),
                new ReSTLine(),
                new ReSTLine());
        assertEquals(true, MultiLineProcessUtils.processMultiLineMatch(null, '#', target));
    }
}
