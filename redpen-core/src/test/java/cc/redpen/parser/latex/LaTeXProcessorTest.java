/**
 * redpen: a text inspection tool
 * Copyright (c) 2014-2015 Recruit Technologies Co., Ltd. and contributors
 * (see CONTRIBUTORS.md)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.redpen.parser.latex;

import cc.redpen.RedPenException;
import cc.redpen.config.Configuration;
import cc.redpen.model.Document;
import cc.redpen.model.Section;
import cc.redpen.parser.DocumentParser;
import cc.redpen.parser.SentenceExtractor;
import org.junit.jupiter.api.Test;

import java.io.UnsupportedEncodingException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class LaTeXProcessorTest {
    @Test
    void testBasicDocument() throws UnsupportedEncodingException {
        String sampleText = ""
                + "\\documentclass[a4paper]{jsarticle}\n"
                + "\\author{asdasd}\n"
                + "\\title{LaTeX Parser Test}\n"
                + "\\maketitle\n"
                + "\\begin{document}\n"
                + "\\section{About Gekioko.}\n"
                + "Gekioko pun pun maru means very very angry.\n"
                + "\n"
                + "The word also have posive meaning.\n"
                + "\\subsection{About Gunma.}\n"
                + "\n"
                + "Gunma is located at west of Saitama.\n"
                + "\n"
                + "Features:\n"
                + "\\begin{enumerate}\n"
                + "\\item Main City: Gumma City\n"
                + "\\item Capital: 200 Millon\n"
                + "\\end{enumerate}\n"
                + "\n"
                + "Location:\n"
                + "\\begin{itemize}\n"
                + "\\item Japan\n"
                + "\\end{itemize}\n"
                + "\n"
                + "The word also have posive meaning. Hower it is a bit wired.\n"
                + "\\end{document}\n";

        Document doc = createFileContent(sampleText);

        assertNotNull(doc, "doc is null");
        assertEquals(3, doc.size());
        // first section
        final Section firstSection = doc.getSection(0);
        assertEquals(1, firstSection.getHeaderContentsListSize());
        assertEquals("", firstSection.getHeaderContent(0).getContent());
        assertEquals(0, firstSection.getNumberOfLists());
        assertEquals(0, firstSection.getNumberOfParagraphs());
        assertEquals(1, firstSection.getNumberOfSubsections());

        // 2nd section
        final Section secondSection = doc.getSection(1);
        assertEquals(1, secondSection.getHeaderContentsListSize());
        assertEquals("About Gekioko.", secondSection.getHeaderContent(0).getContent());
        assertEquals(6, secondSection.getHeaderContent(0).getLineNumber());
        assertEquals(1, secondSection.getHeaderContent(0).getStartPositionOffset());
        assertEquals(0, secondSection.getNumberOfLists());
        assertEquals(2, secondSection.getNumberOfParagraphs());
        assertEquals(1, secondSection.getNumberOfSubsections());
        assertEquals(firstSection, secondSection.getParentSection());

        // validate paragraph in 2nd section
        assertEquals(1, secondSection.getParagraph(0).getNumberOfSentences());
        assertEquals(true, secondSection.getParagraph(0).getSentence(0).isFirstSentence());
        assertEquals(7, secondSection.getParagraph(0).getSentence(0).getLineNumber());
        assertEquals(0, secondSection.getParagraph(0).getSentence(0).getStartPositionOffset());
        assertEquals(1, secondSection.getParagraph(1).getNumberOfSentences());
        assertEquals(true, secondSection.getParagraph(1).getSentence(0).isFirstSentence());
        assertEquals(9, secondSection.getParagraph(1).getSentence(0).getLineNumber());
        assertEquals(0, secondSection.getParagraph(1).getSentence(0).getStartPositionOffset());

        // 3rd section
        Section lastSection = doc.getSection(doc.size() - 1);
        // TBD: LaTeX parser cannot handle lists yet
        //assertEquals(1, lastSection.getNumberOfLists());
        //assertEquals(5, lastSection.getListBlock(0).getNumberOfListElements());
        //assertEquals(2, lastSection.getNumberOfParagraphs());
        assertEquals(7, lastSection.getNumberOfParagraphs());
        assertEquals(1, lastSection.getHeaderContentsListSize());
        assertEquals(0, lastSection.getNumberOfSubsections());
        assertEquals("About Gunma.", lastSection.getHeaderContent(0).getContent());
        assertEquals(1, lastSection.getHeaderContent(0).getStartPositionOffset());
        assertEquals(secondSection, lastSection.getParentSection());

        // validate paragraphs in last section
        // TBD: LaTeX parser cannot handle lists yet; groks as plain sentences results
        //assertEquals(3, lastSection.getParagraph(0).getNumberOfSentences());
        //assertEquals(true, lastSection.getParagraph(0).getSentence(0).isFirstSentence());
        //assertEquals(7, lastSection.getParagraph(0).getSentence(0).getLineNumber());
        //assertEquals(0, lastSection.getParagraph(0).getSentence(0).getStartPositionOffset());
        //assertEquals(2, lastSection.getParagraph(1).getNumberOfSentences());
        //assertEquals(true, lastSection.getParagraph(1).getSentence(0).isFirstSentence());
        //assertEquals(15, lastSection.getParagraph(1).getSentence(0).getLineNumber());
        //assertEquals(false, lastSection.getParagraph(1).getSentence(1).isFirstSentence());
        //assertEquals(15, lastSection.getParagraph(1).getSentence(1).getLineNumber());
        //assertEquals(36, lastSection.getParagraph(1).getSentence(1).getStartPositionOffset());

        assertEquals(1, lastSection.getParagraph(0).getNumberOfSentences());
        assertEquals(true, lastSection.getParagraph(0).getSentence(0).isFirstSentence());
        assertEquals(12, lastSection.getParagraph(0).getSentence(0).getLineNumber());
        assertEquals(0, lastSection.getParagraph(0).getSentence(0).getStartPositionOffset());
        assertEquals(1, lastSection.getParagraph(1).getNumberOfSentences());
        assertEquals(true, lastSection.getParagraph(1).getSentence(0).isFirstSentence());
        assertEquals(14, lastSection.getParagraph(1).getSentence(0).getLineNumber());
        assertEquals(1, lastSection.getParagraph(2).getNumberOfSentences());
        assertEquals(true, lastSection.getParagraph(2).getSentence(0).isFirstSentence());
        assertEquals(16, lastSection.getParagraph(2).getSentence(0).getLineNumber());
        assertEquals(5, lastSection.getParagraph(2).getSentence(0).getStartPositionOffset());
        assertEquals(1, lastSection.getParagraph(3).getNumberOfSentences());
        assertEquals(true, lastSection.getParagraph(3).getSentence(0).isFirstSentence());
        assertEquals(17, lastSection.getParagraph(3).getSentence(0).getLineNumber());
        assertEquals(1, lastSection.getParagraph(4).getNumberOfSentences());
        assertEquals(true, lastSection.getParagraph(4).getSentence(0).isFirstSentence());
        assertEquals(20, lastSection.getParagraph(4).getSentence(0).getLineNumber());
        assertEquals(0, lastSection.getParagraph(4).getSentence(0).getStartPositionOffset());
        assertEquals(1, lastSection.getParagraph(5).getNumberOfSentences());
        assertEquals(true, lastSection.getParagraph(5).getSentence(0).isFirstSentence());
        assertEquals(22, lastSection.getParagraph(5).getSentence(0).getLineNumber());
        assertEquals(2, lastSection.getParagraph(6).getNumberOfSentences());
        assertEquals(true, lastSection.getParagraph(6).getSentence(0).isFirstSentence());
        assertEquals(25, lastSection.getParagraph(6).getSentence(0).getLineNumber());
        assertEquals(0, lastSection.getParagraph(6).getSentence(0).getStartPositionOffset());
        assertEquals(false, lastSection.getParagraph(6).getSentence(1).isFirstSentence());
        assertEquals(25, lastSection.getParagraph(6).getSentence(1).getLineNumber());
        assertEquals(34, lastSection.getParagraph(6).getSentence(1).getStartPositionOffset());
    }

    private Document createFileContent(String inputDocumentString) {
        Configuration conf = Configuration.builder().build();
        DocumentParser parser = DocumentParser.LATEX;
        Document doc = null;
        try {
            doc = parser.parse(inputDocumentString, new SentenceExtractor(conf.getSymbolTable()), conf.getTokenizer());
        } catch (RedPenException e) {
            e.printStackTrace();
        }
        return doc;
    }

}
